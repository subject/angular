# Integrating API Through HTTP GET Request

## Adding Http Modules

For Making an Http Request in angular, `@angular/http` module is required.
Make sure you are installing the latest version of the module as per your project requirements.
It is installed by importing it in your package.json dependency and install it by your package manager.

## Importing the Http Module to the Service

Services in Angular are used for the purpose of providing the data for the components.
In your Service, import the http and response from the http module  

```typescript
import { Http, Response } from "@angular/http";
```

You can now provide accessibility of the http in constructor.

```typescript
constructor(private _http: Http) {}
```

## Writing Get Request

In our Service Class, we will provide a function through which we return the data from the `_http` i.e we are using the Http from the imported Module. We provided a get request in which we are making a request to get the data from the url. Now after providing the get request, we are mapping the whatever response we get into our required format. Here for Example, it's json. As we seen, we are imported `Response` which serves this purpose. We are soon going to utilize this function in our component.   

```typescript
getRequestFunction() {
  return this._http.get('url').map(response => response.json());
}
```

> From Component, before requesting the serivce to provide data by calling this function don't forget to provide the serivce in your main module.

Import the Service in your main app module and place it in the providers of the `@NgModule` to use the serivce over the module.

```typescript
import{ MyService } from './services/myservice.service';
```

```typescript
providers: [ MyService ]
```

Now you are ready to go and use the service.

## Importing the Service in Component

As of now, we are provided the service in the main module. So inorder to use the service in the component,  it must be imported. Now assign a variable to the Service in the constructor of your component.

```typescript
import{ MyService } from './services/myservice.service';
```
```typescript
constructor( private _myService: MyService) {}
```

## Accessing the function in service and getting data

It's time to get the data to your component and the getRequestFunction can be called based on our requirement. Whether on `ngOninit()` or on any function in which we might need the data after some execution.

Whereever it is, we subscribe the data by the getRequestFunction (Through which we make a Http Request) from the service. we can get the data that we can pass it in our data.

```typescript
this._myService.getRequestFunction().subscribe(data => {this.requestedData = data });
```
